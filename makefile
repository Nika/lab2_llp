all: grammar.y aql_lexer.l
	make aql_prog
	make run

aql_prog: src/ast_tree.c src/print_util.c
	bison -d grammar.y 
	flex aql_lexer.l
	gcc -g -o $@ grammar.tab.c lex.yy.c $^

run:
	./aql_prog
