%{
  #include <stdio.h>

    #define YYERROR_VERBOSE 1
    #include <string.h>
    #include "include/ast_tree.h"
    #include "include/print_util.h"

    extern int yylex();
    extern int yylineno;
    void yyerror(const char*);

%}
%define parse.error verbose

%union {
    char* str_type;
    float float_type;
    int int_type;
    int bool_type;
    int cmp_type;
    int value_type;
    int logic_type;
    struct ast_tree_node* node;
}

%token FOR_OP
%token INTO_OP
%token IN_OP
%token CREATE_OP
%token DROP_OP
%token UPDATE_OP
%token INSERT_OP
%token FILTER_OP
%token REMOVE_OP
%token WITH_OP
%token RETURN_OP

%token F_BRACKET_OPEN
%token F_BRACKET_CLOSE
%token COLON
%token SEMICOLON
%token BRACKET_OPEN
%token BRACKET_CLOSE
%token COMMA
%token DOT
%token QUOT_MARK

%token COMPARE_OP
%left COMPARE_OP

%token AND_OP
%token OR_OP
%left OR_OP
%left AND_OP

%token NUMBER_INT
%token NUMBER_FLOAT
%token BOOL_VAL
%token STRING_VAL

%token INT_TYPE
%token FLOAT_TYPE
%token BOOL_TYPE
%token STRING_TYPE

%type<node> query_body
%type<node> insert_query
%type<node> drop_query
%type<node> create_query
%type<node> join_query
%type<node> for_query
%type<node> row_name
%type<node> table_name
%type<node> column_name
%type<node> pair_of_values
%type<node> parameters
%type<node> subquery
%type<node> condition_query
%type<node> cmp_query
%type<node> simple_type
%type<node> simple_value
%type<str_type> string_sentence

%type<int_type> NUMBER_INT
%type<float_type> NUMBER_FLOAT
%type<bool_type> BOOL_VAL
%type<str_type> STRING_VAL
%type<cmp_type> COMPARE_OP
%type<value_type> INT_TYPE
%type<value_type> FLOAT_TYPE
%type<value_type> BOOL_TYPE
%type<value_type> STRING_TYPE
%type<logic_type> OR_OP
%type<logic_type> AND_OP



%%
  query:
      query_body SEMICOLON { print_tree($1, 0); free_tree($1); printf("> "); }
  |   query query_body SEMICOLON { print_tree($2, 0); free_tree($2); printf("> "); }    
  ;

  query_body:
      for_query { $$ = $1; }
  |   create_query { $$ = $1; } 
  |   drop_query { $$ = $1; }
  |   insert_query { $$ = $1; }
  |   join_query { $$ = $1; }
  ;

  insert_query:
      INSERT_OP F_BRACKET_OPEN parameters F_BRACKET_CLOSE IN_OP table_name { $$ = create_subquery_tree_node($3, $6, INSERT_NODE);}
  ;

  drop_query:
      DROP_OP table_name { $$ = create_one_child_tree_node($2, DROP_NODE);}
  ;

  create_query:
      CREATE_OP table_name F_BRACKET_OPEN parameters F_BRACKET_CLOSE  { $$ = create_subquery_tree_node($2, $4, CREATE_NODE);}
  ;

  join_query:
      FOR_OP row_name IN_OP table_name FOR_OP row_name IN_OP table_name FILTER_OP condition_query subquery { $$ = create_nested_loop_tree_node($2, $4, $6, $8, $10, $11);}
  ;

  for_query:
      FOR_OP row_name IN_OP table_name FILTER_OP condition_query subquery { $$ = create_for_tree_node($2, $4, $6, $7);}
  ;

  row_name:
      STRING_VAL{$$ = create_one_child_tree_node(create_name_tree_node($1), ROW_NODE);}
  ;

  table_name:
      STRING_VAL{$$ = create_one_child_tree_node(create_name_tree_node($1), TABLE_NODE);}
  ;

  column_name:
      STRING_VAL DOT STRING_VAL {$$ = create_one_child_tree_node(create_name_tree_node($3), COLUMN_NODE);}
  |   STRING_VAL {$$ = create_one_child_tree_node(create_name_tree_node($1), COLUMN_NODE);}     
  ;

  pair_of_values:
      column_name COLON simple_value { $$ = create_parameter_pair_tree_node($1, $3); }
  |   column_name COLON simple_type { $$ = create_parameter_pair_tree_node($1, $3); }
  ;

  parameters:
      parameters COMMA pair_of_values { $$ = create_parameter_tree_node($1, $3); }
  |   pair_of_values {$$ = create_parameter_tree_node(NULL, $1);}
  ;

  subquery:
      UPDATE_OP row_name WITH_OP F_BRACKET_OPEN parameters F_BRACKET_CLOSE IN_OP table_name {$$ = create_update_tree_node($2, $5, $8);}
    | REMOVE_OP row_name IN_OP table_name {$$ = create_subquery_tree_node($2, $4, REMOVE_NODE);}
    | RETURN_OP row_name COMMA row_name {$$ = create_subquery_tree_node($2, $4, JOIN_NODE);}
    | RETURN_OP row_name {$$ = create_subquery_tree_node($2, NULL, SELECT_NODE);}
  ;

  condition_query:
     BRACKET_OPEN condition_query BRACKET_CLOSE {$$ = $2;}
  |  cmp_query {$$ = $1;}
  |  condition_query AND_OP condition_query {$$ = create_filter_tree_node($1, $2, $3);}
  |  condition_query OR_OP condition_query {$$ = create_filter_tree_node($1, $2, $3);}  
  |  BOOL_VAL{$$ = create_bool_tree_node($1);}
  ;

  cmp_query:
      simple_value COMPARE_OP simple_value {$$ = create_cmp_tree_node($1, $2, $3);}
  |   simple_value COMPARE_OP column_name {$$ = create_cmp_tree_node($1, $2, $3);}
  |   column_name COMPARE_OP simple_value {$$ = create_cmp_tree_node($1, $2, $3);}
  |   column_name COMPARE_OP column_name {$$ = create_cmp_tree_node($1, $2, $3);}
  ;

  simple_type:
      INT_TYPE {$$ = create_type_tree_node($1);}
  |   FLOAT_TYPE {$$ = create_type_tree_node($1);}
  |   BOOL_TYPE {$$ = create_type_tree_node($1);}
  |   STRING_TYPE {$$ = create_type_tree_node($1);}
  ;

  simple_value:
      NUMBER_INT {$$ = create_int_tree_node($1);}
  |   NUMBER_FLOAT {$$ = create_float_tree_node($1);}
  |   BOOL_VAL{$$ = create_bool_tree_node($1);}
  |   QUOT_MARK string_sentence QUOT_MARK {$$ = create_string_tree_node($2);}
  ;

  string_sentence:
      string_sentence STRING_VAL { $$ = strcat($1, $2); }
  |   STRING_VAL { $$ = $1; }    
  ;

%%
void yyerror (const char *s) {
   fprintf (stderr, "%s on line number %d", s, yylineno);
}

int main() {
	printf("> ");
	yyparse();
	return 0;
}


