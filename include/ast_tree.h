#pragma once

#define MAX_CHILD_COUNT 10
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>


enum node_type{
    FOR_NODE,
    CREATE_NODE,
    DROP_NODE,
    UPDATE_NODE,
    INSERT_NODE,
    SELECT_NODE,
    FILTER_NODE,
    REMOVE_NODE,
    JOIN_NODE,
    COMPARE_NODE,
    LOGIC_OP_NODE,
    VALUE_NODE_INT,
    VALUE_NODE_FLOAT,
    VALUE_NODE_STR,
    VALUE_NODE_BOOL,
    TYPE_NODE,
    NAME_NODE,
    NESTED_LOOP_NODE,
    ROW_NODE,
    TABLE_NODE,
    COLUMN_NODE,
    PARAM_NODE,
    PAIR_NODE,
};

enum value_type {
    INT,
    FLOAT,
    BOOL,
    STRING
};

enum compare_type {
    GREATER,
    LESS,
    GREATE_OR_EQUAL,
    LESS_OR_EQUAL,
    EQUAL,
    NOT_EQUAL
};

enum logic_op_type {
    AND,
    OR
}; 

union value {
    int int_value;
    float float_value;
    char* str_value;
    bool bool_value;
    enum compare_type compare_type;
    enum logic_op_type logic_type;
    enum value_type value_type;
};

struct ast_tree_node{
    enum node_type type;
    union value value; 
    int child_count;
    struct ast_tree_node* child_nodes[MAX_CHILD_COUNT];
};

struct ast_tree_node* create_subquery_tree_node(struct ast_tree_node* child_node1, struct ast_tree_node* child_node2, enum node_type type);

struct ast_tree_node* create_one_child_tree_node(struct ast_tree_node* node, enum node_type type);

struct ast_tree_node* create_nested_loop_tree_node(struct ast_tree_node* row_node1, struct ast_tree_node* table_node1, struct ast_tree_node* row_node2, struct ast_tree_node* table_node2, struct ast_tree_node* cond_node, struct ast_tree_node* sub_node);

struct ast_tree_node* create_for_tree_node(struct ast_tree_node* row_node, struct ast_tree_node* table_node, struct ast_tree_node* cond_node, struct ast_tree_node* sub_node);

struct ast_tree_node* create_name_tree_node(char* string);

struct ast_tree_node* create_parameter_pair_tree_node(struct ast_tree_node* column_node, struct ast_tree_node* value_node);

struct ast_tree_node* create_parameter_tree_node(struct ast_tree_node* param_node, struct ast_tree_node* pair_node);

struct ast_tree_node* create_update_tree_node(struct ast_tree_node* row_node, struct ast_tree_node* param_node, struct ast_tree_node* table_node);

struct ast_tree_node* create_filter_tree_node(struct ast_tree_node* cond_node1, int op_type, struct ast_tree_node* cond_node2);

struct ast_tree_node* create_cmp_tree_node(struct ast_tree_node* val_node1, int cmp_type, struct ast_tree_node* val_node2);

struct ast_tree_node* create_type_tree_node(int val_type);

struct ast_tree_node* create_int_tree_node(int val);

struct ast_tree_node* create_float_tree_node(float val);

struct ast_tree_node* create_bool_tree_node(int val);

struct ast_tree_node* create_string_tree_node(char* val);

void free_ast_tree(struct ast_tree_node* tree);

static char* node_names[] = {
    "FOR_NODE",
    "CREATE_NODE",
    "DROP_NODE",
    "UPDATE_NODE",
    "INSERT_NODE",
    "SELECT_NODE",
    "FILTER_NODE",
    "REMOVE_NODE",
    "JOIN_NODE",
    "COMPARE_NODE",
    "LOGIC_OP_NODE",
    "VALUE_NODE_INT",
    "VALUE_NODE_FLOAT",
    "VALUE_NODE_STR",
    "VALUE_NODE_BOOL",
    "TYPE_NODE",
    "NAME_NODE",
    "NESTED_LOOP_NODE",
    "ROW_NODE",
    "TABLE_NODE",
    "COLUMN_NODE",
    "PARAM_NODE",
    "PAIR_NODE",
};

static char* value_type_names[] = { "INT", "FLOAT", "BOOL", "STRING" };

static char* compare_type_names[] = { "GREATER", "LESS", "GREATE_OR_EQUAL", "LESS_OR_EQUAL", "EQUAL", "NOT_EQUAL" };

static char* logic_op_type_names[] = { "AND", "OR" }; 

static enum value_type value_type_enums[] = { INT, FLOAT, BOOL, STRING };

static enum compare_type compare_type_emuns[] = { GREATER, LESS, GREATE_OR_EQUAL, LESS_OR_EQUAL, EQUAL, NOT_EQUAL };

static enum logic_op_type logic_op_type_enums[] = { AND, OR }; 