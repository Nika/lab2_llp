#include "../include/print_util.h"

void print_tree(struct ast_tree_node* cur_node, int indent) {
    if (cur_node == NULL) {
        printf("No tree found\n");
    } else {
        char* space_indent = malloc(sizeof(char) * (indent + 1));
        for (int i = 0; i < indent; i++) {
            space_indent[i] = '\t';
        }
        space_indent[indent] = '\0';
        switch (cur_node->type) {
            case COMPARE_NODE:
                printf("%s%s\n", space_indent, compare_type_names[cur_node->value.compare_type]);
                break;
            case LOGIC_OP_NODE:
                printf("%s%s\n", space_indent, logic_op_type_names[cur_node->value.logic_type]);
                break;
            case TYPE_NODE:
                printf("%s%s\n", space_indent, value_type_names[cur_node->value.value_type]);
                break;
            case NAME_NODE:
                printf("%s%s: %s\n", space_indent,  "name", cur_node->value.str_value);
                break;
            case VALUE_NODE_INT:
                printf("%svalue: %d\n", space_indent, cur_node->value.int_value);
                break;            
            case VALUE_NODE_BOOL:
                printf("%svalue: %s\n", space_indent, cur_node->value.bool_value ? "true" : "false");
                break;
            case VALUE_NODE_STR:
                printf("%svalue: %s\n", space_indent, cur_node->value.str_value);
                break;
            case VALUE_NODE_FLOAT:
                printf("%svalue: %f\n", space_indent, cur_node->value.float_value);
                break;     
            default:
                printf("%s%s\n", space_indent, node_names[cur_node->type]);
                break;
        }

        for (int i = 0; i < cur_node->child_count; i++) {
            print_tree(cur_node->child_nodes[i], indent + 1);
        }

        free(space_indent);
    }
}