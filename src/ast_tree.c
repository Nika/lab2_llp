#include "../include/ast_tree.h"

struct ast_tree_node* init_node() {
    struct ast_tree_node* new_node = (struct ast_tree_node*) malloc(sizeof(struct ast_tree_node));
    new_node->child_count = 0;
    return new_node; 
}

void add_child(struct ast_tree_node* parent_node, struct ast_tree_node* child_node) {
    if (!child_node) {return;}
    parent_node->child_nodes[parent_node->child_count] = child_node;
    parent_node->child_count++;
}

struct ast_tree_node* create_subquery_tree_node(struct ast_tree_node* child_node1, struct ast_tree_node* child_node2, enum node_type type) {
    struct ast_tree_node* new_node = init_node();
    add_child(new_node, child_node1);
    add_child(new_node, child_node2);
    new_node->type = type;
    return new_node;
}

struct ast_tree_node* create_one_child_tree_node(struct ast_tree_node* node, enum node_type type) {
    struct ast_tree_node* new_node = init_node();
    add_child(new_node, node);
    new_node->type = type;
    return new_node;
}

struct ast_tree_node* create_nested_loop_tree_node(struct ast_tree_node* row_node1, struct ast_tree_node* table_node1, struct ast_tree_node* row_node2, struct ast_tree_node* table_node2, struct ast_tree_node* cond_node, struct ast_tree_node* sub_node) {
    struct ast_tree_node* new_node = init_node();
    add_child(new_node, row_node1);
    add_child(new_node, table_node1);
    add_child(new_node, row_node2);
    add_child(new_node, table_node2);
    add_child(new_node, create_one_child_tree_node(cond_node, FILTER_NODE));
    add_child(new_node, sub_node);
    new_node->type = NESTED_LOOP_NODE;
    return new_node;
}

struct ast_tree_node* create_for_tree_node(struct ast_tree_node* row_node, struct ast_tree_node* table_node, struct ast_tree_node* cond_node, struct ast_tree_node* sub_node) {
    struct ast_tree_node* new_node = init_node();
    add_child(new_node, row_node);
    add_child(new_node, table_node);
    add_child(new_node, create_one_child_tree_node(cond_node, FILTER_NODE));
    add_child(new_node, sub_node);
    new_node->type = FOR_NODE;
    return new_node;
}

struct ast_tree_node* create_name_tree_node(char* string) {
    struct ast_tree_node* new_node = init_node();
    new_node->value.str_value = string;
    new_node->type = NAME_NODE;
    return new_node;
}

struct ast_tree_node* create_parameter_pair_tree_node(struct ast_tree_node* column_node, struct ast_tree_node* value_node) {
    struct ast_tree_node* new_node = init_node();
    add_child(new_node, column_node);
    add_child(new_node, value_node);
    new_node->type = PAIR_NODE;
    return new_node;
}

struct ast_tree_node* create_parameter_tree_node(struct ast_tree_node* param_node, struct ast_tree_node* pair_node) {
    add_child(pair_node, param_node);
    return pair_node;
}

struct ast_tree_node* create_update_tree_node(struct ast_tree_node* row_node, struct ast_tree_node* param_node, struct ast_tree_node* table_node) {
    struct ast_tree_node* new_node = init_node();
    add_child(new_node, row_node);
    add_child(new_node, table_node);
    add_child(new_node, param_node);
    new_node->type = UPDATE_NODE;
    return new_node;
}

struct ast_tree_node* create_filter_tree_node(struct ast_tree_node* cond_node1, int op_type, struct ast_tree_node* cond_node2) {
    struct ast_tree_node* new_node = init_node();
    add_child(new_node, cond_node1);
    add_child(new_node, cond_node2);
    new_node->type = LOGIC_OP_NODE;
    new_node->value.logic_type = logic_op_type_enums[op_type];
    return new_node;
}


struct ast_tree_node* create_cmp_tree_node(struct ast_tree_node* val_node1, int cmp_type, struct ast_tree_node* val_node2) {
    struct ast_tree_node* new_node = init_node();
    add_child(new_node, val_node1);
    add_child(new_node, val_node2);
    new_node->type = COMPARE_NODE;
    new_node->value.compare_type = compare_type_emuns[cmp_type];
    return new_node;
}

struct ast_tree_node* create_type_tree_node(int val_type) {
    struct ast_tree_node* new_node = init_node();
    new_node->type = TYPE_NODE;
    new_node->value.value_type = value_type_enums[val_type];
    return new_node;
}

struct ast_tree_node* create_int_tree_node(int val) {
    struct ast_tree_node* new_node = init_node();
    new_node->type = VALUE_NODE_INT;
    new_node->value.int_value = val;
    return new_node;
}

struct ast_tree_node* create_float_tree_node(float val) {
    struct ast_tree_node* new_node = init_node();
    new_node->type = VALUE_NODE_FLOAT;
    new_node->value.float_value = val;
    return new_node;
}


struct ast_tree_node* create_bool_tree_node(int val) {
    struct ast_tree_node* new_node = init_node();
    new_node->type = VALUE_NODE_BOOL;
    if (val == 1) {
        new_node->value.bool_value = true;
    } else {
        new_node->value.bool_value = false;
    }
    return new_node;
}

struct ast_tree_node* create_string_tree_node(char* val) {
    struct ast_tree_node* new_node = init_node();
    new_node->type = VALUE_NODE_STR;
    new_node->value.str_value = val;
    return new_node;
}

void free_ast_tree(struct ast_tree_node* cur_node) {
    for (int i = 0; i < cur_node->child_count; i++) {
        free_ast_tree(cur_node->child_nodes[i]);
    }
    free(cur_node);
}