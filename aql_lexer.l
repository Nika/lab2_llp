%option noyywrap
%option caseless
%option yylineno
%option nounput 
%option noinput 

%{
    #include <stdio.h>
    #include <string.h>

    #include "grammar.tab.h"
    void print_error(char*);
%}

INTEGER [-+]?[0-9]+
FLOAT [-+]?[0-9]+[.][0-9]+
BOOL true|false
STRING [a-zA-Z_][a-zA-Z0-9_]*

%%
FOR                     { return FOR_OP; }
IN                      { return IN_OP; }
CREATE                  { return CREATE_OP; }
DROP                    { return DROP_OP; }
UPDATE                  { return UPDATE_OP; }
INSERT                  { return INSERT_OP; }
FILTER                  { return FILTER_OP; }
REMOVE                  { return REMOVE_OP; }
WITH                    { return WITH_OP; }
RETURN                  { return RETURN_OP; }
INTO                    { return INTO_OP; }

INT                     { yylval.value_type = 0; return INT_TYPE; }
FLOAT                   { yylval.value_type = 1; return FLOAT_TYPE; }
BOOL                    { yylval.value_type = 2; return BOOL_TYPE; }
STRING                  { yylval.value_type = 3; return STRING_TYPE; }

"{"                     { return F_BRACKET_OPEN; }
"}"                     { return F_BRACKET_CLOSE; }
":"                     { return COLON; }
";"                     { return SEMICOLON; }
"("                     { return BRACKET_OPEN; }
")"                     { return BRACKET_CLOSE; }
","                     { return COMMA; }
"."                     { return DOT; }
"\""                    { return QUOT_MARK; }

">"                     { yylval.cmp_type = 0; return COMPARE_OP; }
"<"                     { yylval.cmp_type = 1; return COMPARE_OP; }
">="                    { yylval.cmp_type = 2; return COMPARE_OP; }
"<="                    { yylval.cmp_type = 3; return COMPARE_OP; }
"=="                    { yylval.cmp_type = 4; return COMPARE_OP; }
"!="                    { yylval.cmp_type = 5; return COMPARE_OP; }

"&&"                    { yylval.logic_type = 0; return AND_OP; }
"||"                    { yylval.logic_type = 1; return OR_OP; }


{FLOAT}                 { yylval.float_type = atof(yytext); return NUMBER_FLOAT; }
{INTEGER}               { yylval.int_type = atoi(yytext); return NUMBER_INT; }
{BOOL}                  { yylval.bool_type = (strcmp(yytext, "true")==0 || strcmp(yytext, "TRUE")==0); return BOOL_VAL; }
{STRING}                { yylval.str_type = strdup(yytext); return STRING_VAL; }
[\n\t ]                 { }
.                       { print_error(yytext); }
%%


void print_error(char* msg) {
    printf("Error: %s is not a token \n", msg);
}
